package fr.umfds.TP4suite_Couverture_Mutation;


public class SUT {
	private int[] tab;
	private int taille=0;

	public SUT(int tailleMax) {
		tab=new int[tailleMax];
	}

	public void ajout(int e) throws TableauPleinException{
		if (taille==tab.length) {
			throw new TableauPleinException();
		}
		tab[taille]=e;
		taille++;
	}

	public int[] values() {
		int[] result=new int[taille];
		for (int i=0;i<taille;i++) {
				result[i]=tab[i];
		}
		return result;
	}

	@SuppressWarnings("null")
	public int retourneEtSupprimePremiereOccurenceMin() throws TableauVideException{
		if (taille==0) { //si vide
			throw new TableauVideException();
		}
		int min = tab[0]; //min ddevient le premier case du tableau
		int imin = 0; //et imin est lindice de ce num
		for (int i=1;i<taille;i++) {  //on va parcourir le tabeau
			if (tab[i] <= min) {  //si c'est plus petit alors le min deivent le numero et lindice change
				// on a un nouveau min
				min = tab[i];
				imin = i;
			}
		}
		//on va ensuite une fois qu'on a enlever al valeur
		for (int i=taille-1;i>imin;i--) { // parcours du tableau depuis la fin pour décalage à gauche
			tab[i-1] = tab[i]; // suppression et décalage à gauche
		}
		tab[taille-1] = 0;
		taille--;
		return min;
	}
}